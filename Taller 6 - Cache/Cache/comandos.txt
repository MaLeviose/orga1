--------- EJERCICIO 1 ---------

from cache import *
memory = [0 for i in range(2**16)]
c = CacheCorrespondenciaDirecta(memory=memory, cacheSize=64, nLines=4)

c.fetch(0x0009)
c.fetch(0x001D)
c.fetch(0x000A)
c.fetch(0x0101)
c.fetch(0x0113)
c.fetch(0x000A)
c.fetch(0x001E)
c.fetch(0x0102)
c.fetch(0x0114)

c.mostrarLog()

otro = CacheTotalmenteAsociativa(memory=memory, cacheSize=64,
nLines=4, cacheAlg=FIFO)

otro.fetch(0x0009)
otro.fetch(0x001D)
otro.fetch(0x000A)
otro.fetch(0x0101)
otro.fetch(0x0113)
otro.fetch(0x000A)
otro.fetch(0x001E)
otro.fetch(0x0102)
otro.fetch(0x0114)

otro.mostrarLog()


--------- EJERCICIO 2 ---------

memoria = [0 for i in range(2**16)]

fifo = CacheTotalmenteAsociativa(memory=memoria, cacheSize=64,
nLines=4, cacheAlg=FIFO)
fifo.fetchFrom('benchmark_matrix_iguales.list')
fifo.hitRate()

random = CacheTotalmenteAsociativa(memory=memoria, cacheSize=64,
nLines=4, cacheAlg=RANDOM)
random.fetchFrom('benchmark_matrix_iguales.list')
random.hitRate()

lru = CacheTotalmenteAsociativa(memory=memoria, cacheSize=64,
nLines=4, cacheAlg=LRU)
lru.fetchFrom('benchmark_matrix_iguales.list')
lru.hitRate()

-------------------

fifo = CacheTotalmenteAsociativa(memory=memoria, cacheSize=64,
nLines=4, cacheAlg=FIFO)
fifo.fetchFrom('benchmark_matrix_mix.list')
fifo.hitRate()

random = CacheTotalmenteAsociativa(memory=memoria, cacheSize=64,
nLines=4, cacheAlg=RANDOM)
random.fetchFrom('benchmark_matrix_mix.list')
random.hitRate()

lru = CacheTotalmenteAsociativa(memory=memoria, cacheSize=64,
nLines=4, cacheAlg=LRU)
lru.fetchFrom('benchmark_matrix_mix.list')
lru.hitRate()



